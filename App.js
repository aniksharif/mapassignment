import React, { PureComponent, Component } from 'react';
import { AppRegistry, StyleSheet, View, Dimensions, TouchableOpacity, Text, ScrollView, TextInput, StatusBar } from 'react-native';
import MapView, { PROVIDER_GOOGLE, AnimatedRegion, Animated } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import RetroMapStyles from './MapStyle/retro.json';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const GOOGLE_MAPS_APIKEY = 'AIzaSyAsBXxe6oaC1L98QT6TqvfNJuclKSKWq0w'
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class PlaceAutocompleteInput extends PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GooglePlacesAutocomplete
        placeholder='Search'
        minLength={2} // minimum length of text to search
        autoFocus={false}
        enablePoweredByContainer={false}
        returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        listViewDisplayed='auto'    // true/false/undefined
        fetchDetails={true}
        renderDescription={row => row.description} // custom description render
        onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          this.props.onPress && this.props.onPress(data, details);
        }}

        getDefaultValue={() => ''}

        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          key: GOOGLE_MAPS_APIKEY,
          language: 'en', // language of the results
          types: 'geocode' // default: 'geocode'
        }}

        styles={{
          textInputContainer: {
            width: '100%'
          },
          description: {
            fontWeight: 'bold'
          },
          predefinedPlacesDescription: {
            color: '#1faadb'
          }
        }}

        // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
        // currentLocationLabel="Current location"
        nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        GoogleReverseGeocodingQuery={{
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }}
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: 'distance',
          types: 'food'
        }}

        filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        // predefinedPlaces={[homePlace, workPlace]}

        debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
        renderLeftButton={() => <Text style={{ marginTop: 10, marginLeft: 5 }}>{this.props.label}</Text>}
      />
    )
  }
}
export default class MapExample extends PureComponent {
  constructor() {
    super();
    this.state = {
      coordinates: [
        {
          latitude: 37.3317876,
          longitude: -122.0054812,
        },
        {
          latitude: 37.771707,
          longitude: -122.4053769,
        },
      ],
    };
    this.state.from_lat =  37.3317876 ;
    this.state.from_lng =  -122.0054812;

    this.mapView = null;
  }
  componentWillReceiveProps(props) {
    this.setState({
      from_lat: props.lat || this.state.from_lat,
      from_lng: props.lng || this.state.from_lng,
    });
  }

  // @ref https://developers.google.com/places/web-service/details
  onPress(type, data, details) {
    this.setState({ data, details });
    let full_address = details.formatted_address;
    let lat = details.geometry.location.lat;
    let lng = details.geometry.location.lng;
    if (type === 'from')
      this.setState({ full_address, from_lat: lat, from_lng: lng });
    if (type === 'to')
      this.setState({ full_address, to_lat: lat, to_lng: lng });
  }
  componentDidMount() {
    StatusBar.setHidden(true)
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      }
    );
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 200 }}>
          <PlaceAutocompleteInput label={this.props.from || "From"} onPress={(data, details) => this.onPress('from', data, details)} />
          <PlaceAutocompleteInput label={this.props.to || "To"} onPress={(data, details) => this.onPress('to', data, details)} />
        </View>
        <View style={{ width: width, height: height }}>
        <MapView
                        style={{height: height - 200, width: width}}
                        ref={c => this.mapView = c}
                        region={{
                            latitude: this.state.from_lat,
                            longitude: this.state.from_lng,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                    >
          {/* <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.container}
            customMapStyle={RetroMapStyles}
            userLocationAnnotationTitle='My Location'
            scrollEnabled={true}
            showsUserLocation={true}
            zoomControlEnabled={true}
            toolbarEnabled={true}
            cacheEnabled={true}
            initialRegion={this.state.region}
            region={this.state.region}
            zoomEnabled={true}
          // onRegionChange={region => this.setState({ region })}
          // onRegionChangeComplete={region => this.setState({ region })}
          > */}

            <MapView.Marker coordinate={{latitude: this.state.from_lat, longitude: this.state.from_lng}} />
                        {
                            this.state.to_lat && this.state.to_lng &&
                            <MapView.Marker coordinate={{latitude: this.state.to_lat, longitude: this.state.to_lng}} />
                        }
                        {
                            this.state.to_lat && this.state.to_lng &&
                            <MapViewDirections
                                origin={{latitude: this.state.from_lat, longitude: this.state.from_lng}}
                                waypoints={{latitude: this.state.from_lat, longitude: this.state.from_lng}}
                                destination={{latitude: this.state.to_lat, longitude: this.state.to_lng}}
                                apikey={GOOGLE_MAPS_APIKEY}
                                strokeWidth={10}
                                strokeColor="hotpink"
                                onStart={(params) => {
                                    console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                                }}
                                onReady={(result) => {
                                    this.props.onDirection && this.props.onDirection(result.distance, result.duration);
                                    !this.props.onDirection && alert(`Distance: ${Math.round(result.distance * 10) / 10}km, Time: ${Math.round(result.duration)}min`);
                                    this.mapView.fitToCoordinates(result.coordinates, {
                                        edgePadding: {
                                            right: Math.round(width / 20),
                                            bottom: Math.round(height / 20),
                                            left: Math.round(width / 20),
                                            top: Math.round(height / 20),
                                        }
                                    });
                                }}
                                onError={(errorMessage) => {
                                    alert(errorMessage);
                                }}
                            />
                        }
          </MapView>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  }
});
